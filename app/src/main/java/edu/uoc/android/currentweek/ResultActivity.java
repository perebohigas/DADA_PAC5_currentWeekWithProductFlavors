package edu.uoc.android.currentweek;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

import edu.uoc.android.currentweek.utilities.DateUtil;

public class ResultActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        /** Get elements from layout **/
        TextView textView = findViewById(R.id.textView);
        Button button = findViewById(R.id.button);

        /** Get the intent that started this activity and extract the integer **/
        Intent intent = getIntent();
        Integer value = intent.getIntExtra(MainActivity.EXTRA_VALUE,0);

        /** Check the current week's value in a external class **/
        Calendar calendar = Calendar.getInstance();
        DateUtil dateUtil = new DateUtil(calendar);
        boolean result = dateUtil.isTheCurrentWeekNumber(value);


        if (result){
            /** React to a correct answer **/
            /** Set text and color to message **/
            textView.setText(R.string.results_right);
            textView.setTextColor(Color.GREEN);
            /** Set text to the button **/
            button.setText(R.string.results_buttonRight);
            /** Set right answer sound**/
            final MediaPlayer rightAnswer = MediaPlayer.create(this, R.raw.correct_answer);
            /** Play sound **/
            rightAnswer.start();
        } else {
            /** React to a wrong answer **/
            /** Set text and color to message **/
            textView.setText(R.string.results_wrong);
            textView.setTextColor(Color.RED);
            /** Set wrong answer sound**/
            final MediaPlayer wrongAnswer = MediaPlayer.create(this, R.raw.wrong_answer);
            /** Play sound **/
            wrongAnswer.start();
        }
    }



    /** Method called when the user touches de button **/
    public void backToMainActivity(View view){
        /** Create an intent to go to MainActivity **/
        Intent intent = new Intent(this, MainActivity.class);
        /** Start the MainActivity **/
        startActivity(intent);
    }
}